## SQL x NoSQ

### Base de dados relacional (sql)

o modelo de base de dados relacional representa a base de dados em tabelas.

abaixo um exemplo de diagrama relacional:

<img src="https://debugeverything.com/wp-content/uploads/2020/07/UML-Relational-database-example-1024x576.jpeg" width="600" height="200" />

**Origem da imagem:** https://debugeverything.com/diferenca-base-de-dados-relacional-e-nao-relacional/#:~:text=Bancos%20de%20dados%20relacionais%20como,em%20coleções%20de%20documentos%20JSON.

Na imagem acima, podemos ver que o tip ode chave primária para todas as entidades são ID, e que os tipos de relacionamento são:

    1. O usuário pode ter apenas um endereço;
    2. Um usuário pode fazer 1 ou mais pedidos;
    3. O pedido pode ser de 1 ou mais produtos.



Cada linha da tabela é um registro com sua respectiva identificação chamada CHAVE PRIMÁRIA (PK), e suas colunas são atrubutos com seus respectivos valores.
<br>

<img src="https://debugeverything.com/wp-content/uploads/2020/07/relational-database-table-example.002-1024x576.jpeg" width="600" height="200" />

<br>



A maior vantagem de um banco de dados relacional é poder conectar e relacionar os dados entre outras tabelas.


### Banco de dados não relacional (NoSQL) h3

Existem 3 tipos de bancos de dados não relacionais:

<br>

    1. Key-stored values:
        Consiste numa base de dados representada numa coleção de valoires-chave, ou arrays associativos, que se organizam em uma fila.
        A vantagem deste tpo de base de dados é que seus parâmetros são precisos e são mais facilmente acessíveis para serem consultados.
        <br>

<img src="https://debugeverything.com/wp-content/uploads/2020/07/Key-value-stores.003-1024x576.jpeg" width="600" height="200" />



    2. Graph Stores:
        Seu propósito é lidar com conjuntos de dados estruturados, semi-estruturados ou não estruturados. 
        ele ajuda as organizações a assessar integrar e analizar dados de várias fontese.




<img src="https://debugeverything.com/wp-content/uploads/2020/07/Graph-stores-database-1024x576.jpeg" width="600" height="200" />


    3. Column Store: 
        Armazena dados em colunas em vez de linhas, ao contrário do modelo relacional.
        
<img src="https://debugeverything.com/wp-content/uploads/2020/07/Column-stores-1024x576.jpeg" width="600" height="200" />

<br>

***Fonte***: https://debugeverything.com/diferenca-base-de-dados-relacional-e-nao-relacional/#:~:text=Bancos%20de%20dados%20relacionais%20como,em%20coleções%20de%20documentos%20JSON. 







